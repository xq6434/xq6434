import{_ as s,c as n,o as a,O as p}from"./chunks/framework.495a49b8.js";const y=JSON.parse('{"title":"目录结构","description":"","frontmatter":{},"headers":[],"relativePath":"guild/catalog/index.md"}'),e={name:"guild/catalog/index.md"},l=p(`<h1 id="目录结构" tabindex="-1">目录结构 <a class="header-anchor" href="#目录结构" aria-label="Permalink to &quot;目录结构&quot;">​</a></h1><br><br><div class="language-"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki material-theme-palenight"><code><span class="line"><span style="color:#A6ACCD;">project</span></span>
<span class="line"><span style="color:#A6ACCD;">├── .husky                          # Husky</span></span>
<span class="line"><span style="color:#A6ACCD;">├── build                           # 打包</span></span>
<span class="line"><span style="color:#A6ACCD;">├── dist                            # 打包分析文件</span></span>
<span class="line"><span style="color:#A6ACCD;">├── packages                        # 组件库代码</span></span>
<span class="line"><span style="color:#A6ACCD;">│   ├── _util                       # 组件库工具</span></span>
<span class="line"><span style="color:#A6ACCD;">│   ├── components                  # 组件库公共组件</span></span>
<span class="line"><span style="color:#A6ACCD;">├── lib                             # 打包产物</span></span>
<span class="line"><span style="color:#A6ACCD;">├── site                            # 组件库文档</span></span>
<span class="line"><span style="color:#A6ACCD;">├── .eslintrc.js                    # ESLint 配置</span></span>
<span class="line"><span style="color:#A6ACCD;">├── .prettierignore                 # Prettier 忽略配置</span></span>
<span class="line"><span style="color:#A6ACCD;">├── .prettierrc.js                  # Prettier 配置</span></span>
<span class="line"><span style="color:#A6ACCD;">├── README.md                       # 项目说明</span></span>
<span class="line"><span style="color:#A6ACCD;">├── package.json                    # 包管理</span></span>
<span class="line"><span style="color:#A6ACCD;">├── pnpm-lock.yaml                  # pnpm lock</span></span>
<span class="line"><span style="color:#A6ACCD;">├── pnpm-workspace.yaml             # pnpm workspace</span></span>
<span class="line"><span style="color:#A6ACCD;">├── tsconfig.json                   # TypeScript 配置</span></span>
<span class="line"><span style="color:#A6ACCD;">└── vite.config.ts                  # Vite 配置</span></span></code></pre></div>`,4),t=[l];function o(c,i,r,A,C,d){return a(),n("div",null,t)}const D=s(e,[["render",o]]);export{y as __pageData,D as default};
